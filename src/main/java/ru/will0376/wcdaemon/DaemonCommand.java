package ru.will0376.wcdaemon;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public class DaemonCommand extends AbstractCommand {
	public DaemonCommand() {
		super("daemon");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("toggleEnabled").build(), getArgBuilder("forceStart").build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();
		if (!WindCheckBridge.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);
		if (line.hasOption("toggleEnabled")) {
			Configs.enabled = !Configs.enabled;
		} else if (line.hasOption("forceStart")) {
			Events.forceStart = true;
		}
		return build;
	}
}
