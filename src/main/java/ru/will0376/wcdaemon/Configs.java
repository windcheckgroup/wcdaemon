package ru.will0376.wcdaemon;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = WCDaemon.MOD_ID)
@Mod.EventBusSubscriber
public class Configs {
	@Config.RangeInt
	@Config.Comment("In ticks! Use: https://mapmaking.fr/tick/")
	public static int tickCooldown = 72000;

	public static String skipPermission = "wind.daemon.skipcheck";

	public static boolean enabled = true;

	public static String adminNick = "Daemon";

	@SubscribeEvent
	public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
		if (event.getModID().equals(WCDaemon.MOD_ID)) {
			ConfigManager.sync(WCDaemon.MOD_ID, Config.Type.INSTANCE);
		}
	}
}
