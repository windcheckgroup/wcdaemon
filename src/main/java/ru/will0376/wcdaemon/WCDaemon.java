package ru.will0376.wcdaemon;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.Module;
import ru.will0376.windcheckbridge.utils.SubCommands;
import ru.will0376.windcheckbridge.utils.Token;

@Mod(modid = WCDaemon.MOD_ID,
		name = WCDaemon.MOD_NAME,
		version = WCDaemon.VERSION,
		acceptedMinecraftVersions = "[1.12.2]",
		dependencies = "after:windcheckbridge@[3.0,)",
		acceptableRemoteVersions = "*")
public class WCDaemon {

	public static final String MOD_ID = "wcdaemon";
	public static final String MOD_NAME = "WCDaemon";
	public static final String VERSION = "@version@";

	@Mod.Instance(MOD_ID)
	public static WCDaemon INSTANCE;

	public static Module module = Module.builder().setDefaultNames(MOD_NAME).modid(MOD_ID).version(VERSION).build();
	public static Token.Requester requester;
	public static SubCommands subCommands;

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		requester = WindCheckBridge.aVersion.registerModule(module);
		subCommands = WindCheckBridge.aVersion.registerNewCommand(new DaemonCommand());
	}
}
