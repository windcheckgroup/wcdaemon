package ru.will0376.wcdaemon;

import lombok.extern.log4j.Log4j2;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.events.req.RequestScreenEvent;

@Mod.EventBusSubscriber
@Log4j2
public class Events {
	public static boolean forceStart = false;

	@SubscribeEvent
	public static void event(TickEvent.ServerTickEvent event) {
		if (event.phase == TickEvent.Phase.END) {
			if ((FMLCommonHandler.instance()
					     .getMinecraftServerInstance()
					     .getTickCounter() % Configs.tickCooldown == 0 || forceStart) && Configs.enabled) {
				if (forceStart)
					forceStart = false;
				log.debug("~~~Starting daemon check~~~");
				FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers().forEach(player -> {
					String name = player.getName();

					if (WindCheckBridge.aVersion.senderCanUseCommand(player, Configs.skipPermission)) {
						log.debug("Skipped {}", name);
						return;
					}
					log.debug("Send check message to {}", name);
					WindCheckBridge.aVersion.createNewToken(name, Configs.adminNick, WCDaemon.requester);
					WindCheckBridge.aVersion.sendEvent(new RequestScreenEvent(Configs.adminNick, WCDaemon.requester, name));
				});
				log.debug("~~~Done~~~");
			}
		}
	}
}
